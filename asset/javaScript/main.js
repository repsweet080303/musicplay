const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);
const heading = $('header h2');
const cdThumb = $('.cd-thumb');
const audio = $('#audio');
const cd = $('.cd');
const playBtn = $('.btn-toggle-play');
const player = $('.player');
const progress = $('#progress');
const prevBtn = $('.btn-prev');
const nextBtn = $('.btn-next');
const randomBtn = $('.btn-random');
const repeatBtn = $('.btn-repeat');
const playList = $('.playlist');

const app = {
  currentIndex: 0,
  isPlaying: false,
  isRandom: false,
  isRepeat: false,
  songs: [
    {
      name: 'Waiting for you',
      singer: 'MoNo',
      path: './asset/music/Waiting-For-You-MONO-Onionn.mp3',
      image: './asset/img/mono.png',
    },
    {
      name: 'Chạy ngay đi',
      singer: 'Son Tung MTP',
      path: './asset/music/Chay-Ngay-Di-Son-Tung-M-TP.mp3',
      image: './asset/img/run.png',
    },
    {
      name: 'Em là',
      singer: 'MoNo',
      path: './asset/music/Em-La-MONO-Onionn.mp3',
      image: './asset/img/mono.png',
    },
    {
      name: 'Nơi này có anh',
      singer: 'Son Tung MTP',
      path: './asset/music/Noi Nay Co Anh - Son Tung M-TP.mp3',
      image: './asset/img/Nơi_này_có_anh_-_Single_Cover.jpg',
    },
    {
      name: 'Chúng ta của hiện tại',
      singer: 'Son Tung MTP',
      path: './asset/music/Chung ta cua hien tai - DVD MTP.mp3',
      image: './asset/img/Chung_Ta_Cua_Hien_Tai.png',
    },
  ],
  render: function () {
    const htmls = this.songs.map((song, index) => {
      return `<div class="song ${
        index === this.currentIndex ? 'active' : ''
      }" data-index ="${index}" >
      <div
        class="thumb"
        style="
          background-image: url('${song.image}');
        "
      ></div>
      <div class="body">
        <h3 class="title">${song.name}</h3>
        <p class="author">${song.singer}</p>
      </div>
      <div class="option">
        <i class="fas fa-ellipsis-h"></i>
      </div>
    </div>`;
    });
    playList.innerHTML = htmls.join('');
  },
  defineProperties: function () {
    Object.defineProperty(this, 'currentSong', {
      get: function () {
        return this.songs[this.currentIndex];
      },
    });
  },
  handleEvents: function () {
    const _this = this;
    const cdWidth = cd.offsetWidth;
    // Xủ lý đĩa quay
    const cdThumbAnimate = cdThumb.animate(
      [
        {
          transform: 'rotate(360deg)',
        },
      ],
      {
        duration: 10000,
        iterations: Infinity,
      }
    );
    cdThumbAnimate.pause();

    // xử lý thu phóng
    document.onscroll = function () {
      const scrollTop = window.scrollY || document.documentElement.scrollTop;
      const newCdWidth = cdWidth - scrollTop;
      cd.style.width = newCdWidth > 0 ? newCdWidth + 'px' : 0;
      cd.style.opacity = newCdWidth / cdWidth;
    };
    //xử lý khi onclick play
    playBtn.onclick = function () {
      if (_this.isPlaying) {
        audio.pause();
      } else {
        audio.play();
      }
    };
    audio.onplay = function () {
      _this.isPlaying = true;
      player.classList.add('playing');
      cdThumbAnimate.play();
    };
    audio.onpause = function () {
      _this.isPlaying = false;
      player.classList.remove('playing');
      cdThumbAnimate.pause();
    };
    //xủ lý thanh progress
    audio.ontimeupdate = function () {
      if (audio.duration) {
        const progressPercent = Math.floor(
          (audio.currentTime / audio.duration) * 100
        );
        progress.value = progressPercent;
      }
    };
    //xủ lý khi tua nhac

    progress.onchange = function (e) {
      const seekAudio = (audio.duration / 100) * e.target.value;
      audio.currentTime = seekAudio;
    };
    //xu ly chuyen bai hat
    nextBtn.onclick = function () {
      if (_this.isRandom) {
        _this.playRandomSong();
      } else {
        _this.nextSong();
      }
      audio.play();
      _this.render();
      _this.songScrollActive();
    };
    //  xủ lý khi hat lai bai truoc
    prevBtn.onclick = function () {
      if (_this.isRandom) {
        _this.playRandomSong();
      } else {
        _this.prevSong();
      }
      audio.play();
      _this.render();
      _this.songScrollActive();
    };
    //xu ly random nhac
    randomBtn.onclick = function () {
      _this.isRandom = !_this.isRandom;
      randomBtn.classList.toggle('active', _this.isRandom);
    };
    //handleEvents repeat song
    repeatBtn.onclick = function () {
      _this.isRepeat = !_this.isRepeat;
      repeatBtn.classList.toggle('active', _this.isRepeat);
    };
    // handleEvents next song when audio ended
    audio.onended = function () {
      if (_this.isRepeat) {
        audio.play();
      } else {
        nextBtn.click();
      }
    };
    //handleEvents when click playlist
    playList.onclick = function (e) {
      //handleClick in song
      const songNode = e.target.closest('.song:not(.active)');
      if (songNode || e.target.closest('.option')) {
        if (songNode) {
          _this.currentIndex = Number(songNode.dataset.index);
          _this.loadCurrentSong();
          _this.render();
          audio.play();
        }
      }
    };
  },
  loadCurrentSong: function () {
    //console.log(heading,cdThumb,audio);
    heading.textContent = this.currentSong.name;
    cdThumb.style.backgroundImage = `url('${this.currentSong.image}')`;
    audio.src = this.currentSong.path;
  },
  nextSong: function () {
    this.currentIndex++;
    if (this.currentIndex >= this.songs.length) {
      this.currentIndex = 0;
    }
    this.loadCurrentSong();
  },
  prevSong: function () {
    this.currentIndex--;
    if (this.currentIndex < 0) {
      this.currentIndex = this.songs.length - 1;
    }
    this.loadCurrentSong();
  },
  playRandomSong: function () {
    let newIndex;
    do {
      newIndex = Math.floor(Math.random() * this.songs.length);
    } while (newIndex === this.currentIndex);
    //console.log(newIndex);
    this.currentIndex = newIndex;
    this.loadCurrentSong();
  },
  //Scroll top song
  songScrollActive: function () {
    setTimeout(() => {
      $('.song.active').scrollIntoView({
        behavior: 'smooth',
        block: 'nearest',
      });
    }, 500);
  },
  // xu ly repeat song
  playRepeatSong: function (e) {},

  start: function () {
    this.defineProperties();
    this.handleEvents();
    this.loadCurrentSong();

    this.render();
  },
};

app.start();
